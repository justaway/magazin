from django.db import models
from django.conf import settings
from django.contrib.auth.models import AbstractUser

class User(AbstractUser):
    is_regular = models.BooleanField(default=False, null=False)

    def set_regular(self):
        orders = Order.objects.filter(user=self)
        total = 0
        for order in orders:
            total = total + order.total
        
        if (total >= 5000):
            self.is_regular = True
            self.save()

class Item(models.Model): # Таблица товаров
    name = models.CharField(max_length=255, null=False)
    color = models.CharField(max_length=255, null=False)
    price = models.DecimalField(max_digits=10, decimal_places=2, null=False)
    description = models.TextField(max_length=511, default="Текст по умолчанию")
    image = models.ImageField(upload_to="user_images", null=True)
    number = models.PositiveBigIntegerField(default=100)

class Cart(models.Model): # Корзина покупателя
    item = models.ForeignKey(Item, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    amount = models.PositiveSmallIntegerField(default=1, null=False) # Количество предметов в корзине

    def value(self):
        return self.item.price * self.amount

class Order(models.Model): # Таблица оформленных заказов
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    total = models.DecimalField(max_digits=10, decimal_places=2, null=True) # Стоимость всего заказа

class OrderCart(models.Model):
    order = models.ForeignKey(Order, on_delete=models.CASCADE)
    name = models.CharField(max_length=255, null=False)
    color = models.CharField(max_length=255, null=False)
    value = models.DecimalField(max_digits=10, decimal_places=2, null=False) # Стоимость позиции в заказе = цена * количество
    amount = models.PositiveSmallIntegerField(default=1, null=False) # Количество предметов в заказе